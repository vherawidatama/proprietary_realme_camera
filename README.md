# proprietary_vendor_realme_camera

Prebuilt stock Realme Camera to include in custom ROM builds.

### How to use?

1. Clone this repo to `vendor/realme/camera`

2. Inherit it from `device.mk` in device tree:

```
# Camera
$(call inherit-product-if-exists, vendor/realme/camera/realmecamera.mk)
```

3. Ensure that the PRODUCT_BRAND is either oneplus or oppo or realme and that it is not overriden by any of the safetynet hacks.
